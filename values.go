package main

import (
	"errors"
	"fmt"
	"math"
	"regexp"
	"strconv"
)

type points struct {
	rearLeft    float64
	rearCenter  float64
	rearRight   float64
	middleLeft  float64
	middleRight float64
	frontLeft   float64
	frontCenter float64
	frontRight  float64
}

func processPoints(raw string) (*points, error) {
	re := regexp.MustCompile(`\S+`)
	matches := re.FindAllString(raw, -1)
	if len(matches) != 49 {
		return nil, errors.New("invalid input format")
	}

	center, err := getPoint(matches, 24, "center")
	if err != nil {
		return nil, err
	}

	p := &points{}

	if err = p.rear(matches, center); err != nil {
		return nil, err
	}
	if err = p.middle(matches, center); err != nil {
		return nil, err
	}
	if err = p.front(matches, center); err != nil {
		return nil, err
	}

	return p, nil
}

func getPoint(values []string, index int, name string) (float64, error) {
	value, err := strconv.ParseFloat(values[index], 32)
	if err != nil {
		return 0.0, fmt.Errorf("unable to parse %s value: %s", name, err)
	}

	return fixPrecision(value), nil
}

func fixPrecision(value float64) float64 {
	return math.Round(value*100000.0) / 100000.0
}

func (p *points) rear(values []string, center float64) error {
	var err error

	if p.rearLeft, err = getPoint(values, 0, "left rear"); err != nil {
		return err
	}
	if p.rearCenter, err = getPoint(values, 3, "center rear"); err != nil {
		return err
	}
	if p.rearRight, err = getPoint(values, 6, "right rear"); err != nil {
		return err
	}

	p.rearLeft = fixPrecision(p.rearLeft - center)
	p.rearCenter = fixPrecision(p.rearCenter - center)
	p.rearRight = fixPrecision(p.rearRight - center)

	return nil
}

func (p *points) middle(values []string, center float64) error {
	var err error

	if p.middleLeft, err = getPoint(values, 21, "left middle"); err != nil {
		return err
	}
	if p.middleRight, err = getPoint(values, 27, "right middle"); err != nil {
		return err
	}

	p.middleLeft = fixPrecision(p.middleLeft - center)
	p.middleRight = fixPrecision(p.middleRight - center)

	return nil
}

func (p *points) front(values []string, center float64) error {
	var err error

	if p.frontLeft, err = getPoint(values, 42, "left front"); err != nil {
		return err
	}
	if p.frontCenter, err = getPoint(values, 45, "center front"); err != nil {
		return err
	}
	if p.frontRight, err = getPoint(values, 48, "right front"); err != nil {
		return err
	}

	p.frontLeft = fixPrecision(p.frontLeft - center)
	p.frontCenter = fixPrecision(p.frontCenter - center)
	p.frontRight = fixPrecision(p.frontRight - center)

	return nil
}

func noNegative(value float64) float64 {
	if value < 0 {
		value = fixPrecision(value * -1.0)
	}

	return value
}

func (p *points) findWorst() string {
	var value float64
	var name string

	if noNegative(p.rearLeft) > noNegative(value) {
		value = p.rearLeft
		name = "left rear"
	}

	if noNegative(p.rearCenter) > noNegative(value) {
		value = p.rearCenter
		name = "center rear"
	}

	if noNegative(p.rearRight) > noNegative(value) {
		value = p.rearRight
		name = "right rear"
	}

	if noNegative(p.middleLeft) > noNegative(value) {
		value = p.middleLeft
		name = "left middle"
	}

	if noNegative(p.middleRight) > noNegative(value) {
		value = p.middleRight
		name = "right middle"
	}

	if noNegative(p.frontLeft) > noNegative(value) {
		value = p.frontLeft
		name = "left front"
	}

	if noNegative(p.frontCenter) > noNegative(value) {
		value = p.frontCenter
		name = "center front"
	}

	if noNegative(p.frontRight) > noNegative(value) {
		value = p.frontRight
		name = "right front"
	}

	adjust := "Tighten"
	if value < 0 {
		adjust = "Loosen"
	}

	return fmt.Sprintf("%s %s screw by %0.5fmm", adjust, name, noNegative(value))
}

func (p *points) String() string {
	return fmt.Sprintf(
		"% 0.5f\t% 0.5f\t% 0.5f\n"+
			"% 0.5f\t% 0.5f\t% 0.5f\n"+
			"% 0.5f\t% 0.5f\t% 0.5f",
		p.rearLeft, p.rearCenter, p.rearRight, p.middleLeft, 0.0,
		p.middleRight, p.frontLeft, p.frontCenter, p.frontRight,
	)
}
