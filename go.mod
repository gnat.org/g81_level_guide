module mod

go 1.12

require (
	github.com/stretchr/testify v1.3.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20190426135247-a129542de9ae // indirect
)
