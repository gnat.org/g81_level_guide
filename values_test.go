package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestValues(t *testing.T) {
	var actual *points
	var err error

	input := `0.10083 0.25583 0.37111 0.44667 0.48250 0.47861 0.43500
0.13852 0.19855 0.25244 0.30019 0.34179 0.37725 0.40657
0.16352 0.15441 0.16219 0.18685 0.22840 0.28682 0.36213
0.17583 0.12343 0.10037 0.10667 0.14231 0.20731 0.30167
0.17546 0.10559 0.06698 0.05963 0.08355 0.13873 0.22519
0.16241 0.10090 0.06201 0.04574 0.05210 0.08108 0.13269
0.13667 0.10935 0.08546 0.06500 0.04796 0.03435 0.02417`
	expected := &points{
		rearLeft:    0.10083 - 0.10667,
		rearCenter:  0.44667 - 0.10667,
		rearRight:   0.43500 - 0.10667,
		middleLeft:  0.17583 - 0.10667,
		middleRight: 0.30167 - 0.10667,
		frontLeft:   0.13667 - 0.10667,
		frontCenter: 0.06500 - 0.10667,
		frontRight:  0.02417 - 0.10667,
	}

	t.Run("processPoints", func(t *testing.T) {
		actual, err = processPoints(input)
		require.Nil(t, err)
		require.Equal(t, expected, actual)
	})

	t.Run("getPoint", func(t *testing.T) {
		value, err := getPoint([]string{"0.00584"}, 0, "test")
		require.Nil(t, err)
		require.Equal(t, 0.00584, value)
	})

	t.Run("findWorst", func(t *testing.T) {
		require.Equal(t, "Tighten center rear screw by 0.34000mm", actual.findWorst())
	})

	t.Run("String", func(t *testing.T) {
		test := "-0.00584\t 0.34000\t 0.32833\n" +
			" 0.06916\t 0.00000\t 0.19500\n" +
			" 0.03000\t-0.04167\t-0.08250"
		require.Equal(t, test, actual.String())
	})
}
