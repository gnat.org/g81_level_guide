package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/tarm/serial"
)

func main() {
	var port, logFile string
	var baud int
	var skipOK, debug bool

	flag.StringVar(&port, "com-port", "", "Serial TTY for printer communication")
	flag.StringVar(&logFile, "log-file", "", "Optionally log serial data to this file")
	flag.IntVar(&baud, "port-speed", 115200, "Serial baud rate")
	flag.BoolVar(&skipOK, "skip-ok", false, "Skip looking for initial OK")
	flag.BoolVar(&debug, "debug", false, "Print all measurement after each test")

	flag.Parse()

	if port == "" {
		log.Fatalln("You must supply a -com-port")
	}

	var fp *os.File
	if logFile != "" {
		var err error
		fp, err = os.OpenFile(logFile, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
		if err != nil {
			log.Fatalln("Unable to open serial log:", err)
		}
	}
	defer func() {
		if fp != nil {
			fp.Close()
		}
	}()

	sp, err := serial.OpenPort(&serial.Config{
		Name:        port,
		Baud:        baud,
		ReadTimeout: time.Minute * 10,
	})
	if err != nil {
		log.Fatalln("Unable to connect to printer:", err)
	}

	if !skipOK {
		_, err = waitForOK(sp, fp)
		if err != nil {
			log.Fatalln("Unable to locate initial OK:", err)
		}
	}

	var p *points
	buf := bufio.NewReader(os.Stdin)
	for {
		if err = g80(sp, fp); err != nil {
			log.Fatalln("Unable to run bed calibration:", err)
		}

		p, err = g81(sp, fp)
		if err != nil {
			log.Fatalln("Unable to fetch calibration results:", err)
		}

		if debug {
			fmt.Println(p.String())
		}
		log.Println(p.findWorst())

		fmt.Print("Again? (Y/n): ")
		ans, err := buf.ReadBytes('\n')
		if err != nil {
			log.Fatalln("Unable to read answer:", err)
		}
		if ans[0] == 'n' || ans[0] == 'N' {
			break
		}
	}

	log.Println("Final results:")
	fmt.Println(p.String())
}
